import 'dart:core';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutterapp/model/todo.dart';
import 'package:flutterapp/services/todo_service.dart';
import 'package:intl/intl.dart';

class StatScreen extends StatefulWidget {
  @override
  _StatScreenState createState() => _StatScreenState();
}

class _StatScreenState extends State<StatScreen> {

  List<Task> _tasklist = List<Task>();
  List<charts.Series<Task, String>> _seriesPieData;
  var _todoService = TodoService();
  var now;
  var sum;

  duration(String time1, String time2){
    var min1 = int.parse(time1.substring(3, 5));
    var min2 = int.parse(time2.substring(3, 5));
    var hr1 = int.parse(time1.substring(0, 2));
    var hr2 = int.parse(time2.substring(0, 2));

    while (min2 > min1) {
      --hr1;
      min1 += 60;
    }
    var min = min1 - min2;
    var hr = hr1 - hr2;
    min += hr * 60;
    min = - min;
    print('min=$min');
    return min;
  }

  getTask() async{
    print('hi');
    sum = 0;
    _tasklist = List<Task>();
    var tasks = await _todoService.readTodoByDate(DateFormat("dd-MM-yyyy").format(now).toString());
    print('Tasks: $tasks');
    tasks.forEach((task) {
      setState(() {
        var taskModel = Task();

        taskModel.task = task['task'];
        print(taskModel.task);
        print(task['start_Time']);
        print(task['end_Time']);
        //print(duration(task['start_Time'], task['end_Time']));
        taskModel.duration = duration(task['start_Time'], task['end_Time']);
        print(taskModel.duration);
        sum += taskModel.duration;
        print('sum = $sum');
        print(taskModel);
        _tasklist.add(taskModel);

      });
    });
    print(_tasklist);
  }

  _generateData(){
    var pieData = _tasklist;
    _seriesPieData.add(
      charts.Series(
        data: pieData,
        domainFn: (Task task,_) => task.task,
        measureFn: (Task task,_) => task.duration,
        colorFn: (Task task, _) => charts.ColorUtil.fromDartColor(Colors.purple),
        id: 'Daily Task',
        labelAccessorFn: (Task row,_) => '${row.task}\n${(row.duration * 100) /sum}%',
      ),
    );
  }
//  \n${(row.duration * 100 / sum).toStringAsFixed(1)}

  @override
  void initState(){
    super.initState();
    now = DateTime.now();
    getTask();
    _seriesPieData = List<charts.Series<Task, String>>();
    _generateData();
  }

  @override
  Widget build(BuildContext context){
    return Column(
      children: <Widget>[
        Text('Tasks Distribution'),
        SizedBox(height: 10),
        Expanded(
          child: charts.PieChart(
            _seriesPieData,
            animate: true,
            animationDuration: Duration(seconds: 3),
            behaviors: [
              charts.DatumLegend(
                outsideJustification: charts.OutsideJustification.endDrawArea,
                horizontalFirst: false,
                desiredMaxRows: 2,
                cellPadding: EdgeInsets.only(right: 4, bottom: 4),
                entryTextStyle: charts.TextStyleSpec(
                    color: charts.MaterialPalette.purple.shadeDefault,
                    fontFamily: 'Georgia',
                    fontSize: 11
                )
            )
            ],
            defaultRenderer: charts.ArcRendererConfig(
                arcWidth: 100,
                arcRendererDecorators: [charts.ArcLabelDecorator(
                    labelPosition: charts.ArcLabelPosition.inside
                )]
            ),

          ),
        )],
    );
  }

}

class Task{
  String task;
  int duration;

  taskMap(){
    var mapping = Map<String, dynamic>();
    mapping['task'] = task;
    mapping['duration'] = duration;

    return mapping;
  }
}

